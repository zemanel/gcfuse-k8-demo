#!/bin/sh
set -e -x

cmd="$@"

echo "Mounting ${GCS_BUCKET_NAME} bucket"

gcsfuse -o allow_other ${GCS_BUCKET_NAME} /mnt

echo "Executing cmd: ${cmd}"

exec $cmd
