# Based on https://github.com/lev-kuznetsov/gcsfuse-docker
FROM levkuznetsov/gcsfuse-docker

# 6A030B21BA07F4FB
RUN apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 6A030B21BA07F4FB \
    && apt-get update \
    && apt-get install -y python3-pkg-resources python3-watchdog

WORKDIR /

ADD ./entrypoint.sh /usr/local/bin/entrypoint.sh
ADD ./generate-files.sh /usr/local/bin/generate-files.sh

ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]
