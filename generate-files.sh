#!/bin/sh
# Simple file generator
# generate-files.sh <dir>
set -e -x

OUT_DIR=$1

INTERVAL=$2

# create directory of not exists
if [ ! -d "$OUT_DIR" ]; then
  echo "Creating '${OUT_DIR}'"
  mkdir "${OUT_DIR}"
fi

while true;
    FILE="${OUT_DIR}/$(date).txt"
    echo "Writing to ${FILE}"
    do ps aux > ${FILE}
    sleep ${INTERVAL}
done
